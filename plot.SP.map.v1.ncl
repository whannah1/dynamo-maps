load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"

begin
	idir = "/Users/whannah/DYNAMO/"
	odir = "/Users/whannah/DYNAMO/"

	fig_type = "png"
	fig_file = odir+"SP.map"
	
	gspc = 2
	
;====================================================================================================
;====================================================================================================
  wks = gsn_open_wks(fig_type,fig_file)
  	res = True
  	res@gsnDraw = False
  	res@gsnFrame = False
  	
  	res@mpCenterLonF = 0.
  	res@mpCenterLatF = 35.
  	
  	res@mpCenterLonF = -80.
  	res@mpCenterLatF = 25.
  	
  	res@mpProjection      = "Orthographic"
  	res@mpPerimOn         = True
  	res@vpHeightF = 3.
  	res@vpWidthF  = res@vpHeightF
  	
  	res@tmXBOn = False
  	res@tmXTOn = False
  	res@tmYLOn = False
  	res@tmYROn = False
  	
  	res@mpGridAndLimbOn   = True
  	res@mpGridLatSpacingF = gspc   
  	res@mpGridLonSpacingF = gspc

  plot = gsn_csm_map_ce(wks,res) 

  	mres = True
  	mres@gsMarkerIndex = 1
  	mres@gsMarkerSizeF = 0.02
  	mres@gsMarkerColor = "black"
  ;--------------------------------------------------------------------------------------------
  ; Add CRM markers
  ;--------------------------------------------------------------------------------------------
  if True then
  	mres = True
  	mres@gsMarkerIndex = 6
  	mres@gsMarkerSizeF = 0.003
  	mres@gsMarkerColor = "blue"
  csite_lat  = tofloat(ispan(4,48,gspc))
  csite_lon  = tofloat(ispan(-112,-50,gspc))
  ;csite_lat  = tofloat(ispan(0,52,gspc))
  ;csite_lon  = tofloat(ispan(-12,12,gspc))
  num_y = dimsizes(csite_lat)
  num_x = dimsizes(csite_lon)
  if gspc.eq.1 then num_c = 6 end if
  if gspc.eq.2 then num_c = 8 end if
  if gspc.eq.4 then num_c = 16 end if
  dc = 0.6*gspc/num_c
  csite_lon = csite_lon + gspc*0.5
  csite_lat = csite_lat + gspc*0.2
  cdum = new(num_y*num_x*num_c,graphic)
  do y = 0,num_y-1
  do x = 0,num_x-1
  do c = 0,num_c-1
    ;print(""+(csite_lat(y)+x*dc))
    cdum(c+x*num_c+y*num_x*num_c) = gsn_add_polymarker(wks,plot,csite_lon(x),csite_lat(y)+c*dc,mres)
  end do
  end do
  end do
  end if
  ;--------------------------------------------------------------------------------------------
  ; Specify site position
  ;--------------------------------------------------------------------------------------------
  ; Gauge Sites
  ;site_lat  = (/-0.7,		0.,		-7.3,		-7.3,		4.,		6.7,		0.48,		1.8/)
  ;site_lon  = (/73.2,		80.,		80.,		72.5,		73.,		73.2,		72.9,		73.5/)
  ; SSA
  ;site_lat  = (/-0.7,		0.,		-7.3,	-7.3 /)
  ;site_lon  = (/73.2,		80.,		80.,		72.5 /)
  ;site_name = (/"Gan",		"Mirai",	"Revelle",	"Diego Garcia",	"",	"",/)
  ; NSA+SSA
  site_lat  = (/-0.7,		0.,		-7.3,	-7.3 ,   4.1,  6.9,  0./)
  site_lon  = (/73.2,		80.,		80.,		72.5 ,  73.5, 79.8, 80./)
  ;site_name = (/"Gan",		"Mirai",	"Revelle",	"Diego Garcia",	"",	"",/)
  ;--------------------------------------------------------------------------------------------
  ; Add DYNAMO Site Markers
  ;--------------------------------------------------------------------------------------------
  ;num_s = dimsizes(site_lat)
  ;dum = new(num_s,graphic)
  ;do s = 0,num_s-1
  ;  dum(s) = gsn_add_polymarker(wks,plot,site_lon(s),site_lat(s),mres)
  ;end do
  ;--------------------------------------------------------------------------------------------
  ; Add Polygon
  ;--------------------------------------------------------------------------------------------
  if False then
    ;plat1 = -10.
    ;plat2 =  10.
    ;plon1 =  65.
    ;plon2 =  85.
    plat1 =   0.
	plat2 =   6.
	plon1 =  72.
	plon2 =  80.
    pgx = (/plon1,plon1,plon2,plon2,plon1/)
    pgy = (/plat1,plat2,plat2,plat1,plat1/)
    		pgres                	= True
    		pgres@gsFillColor    	= "blue"
    		pgres@gsFillOpacityF		= 0.1
    gdum = gsn_add_polygon(wks,plot,pgx,pgy,pgres)
  end if
  ;--------------------------------------------------------------------------------------------
  ; Add DYNAMO Sounding Array lines
  ;--------------------------------------------------------------------------------------------
  if False then
  	lres = True
  	lres@gsLineColor = "gray"
  ; SSA
  ;array_lat = new(5,float)
  ;array_lon = new(5,float)
  ;array_lat(:3) = site_lat(:3)
  ;array_lon(:3) = site_lon(:3)
  ;array_lat(4) = site_lat(0)
  ;array_lon(4) = site_lon(0)
  ; NSA+SSA
  array_lat = new(8,float)
  array_lon = new(8,float)
  array_lat(:3) = site_lat(:3)
  array_lon(:3) = site_lon(:3)
  array_lat(4) = site_lat(0)
  array_lon(4) = site_lon(0)
  array_lat(5:) = site_lat(4:)
  array_lon(5:) = site_lon(4:)
  ldum = gsn_add_polyline(wks,plot,array_lon,array_lat,lres)
  end if
  ;--------------------------------------------------------------------------------------------
  ; Add Text
  ;--------------------------------------------------------------------------------------------
    	txres               = True
  	txres@txFontHeightF = 0.01
	txres@txFontColor   = "black"
  ;gsn_text_ndc(wks,"Hanimaadhoo",0.41, 0.53,txres)
  ;gsn_text_ndc(wks,"Male",0.44, 0.48,txres)
  ;gsn_text_ndc(wks,"Kadhdhoo",0.43, 0.45,txres)
  ;gsn_text_ndc(wks,"Kaadedhdhoo",0.41, 0.43,txres)
  ;gsn_text_ndc(wks,"Gan",0.44, 0.4,txres)
  ;gsn_text_ndc(wks,"R/V Mirai",0.63, 0.42,txres)
  ;gsn_text_ndc(wks,"Diego Garcia",0.4, 0.28,txres)
  ;gsn_text_ndc(wks,"R/V Revelle",0.64, 0.28,txres)

  draw(plot)
  frame(wks)
  
		print("")
		print("    "+fig_file+"."+fig_type)
		print("")

;====================================================================================================
;====================================================================================================
end
